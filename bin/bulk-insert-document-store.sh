#/bin/sh

# apply preprocessd bulk insert
echo -n 'bulking into elasticsearch index "document-store"..'
curl -H "Content-Type: application/json" -X POST "localhost:9200/document-store/_doc/_bulk?pretty&refresh" --data-binary "@data/bulk-document-store.jsonl"
echo " done."
