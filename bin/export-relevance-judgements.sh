#!/bin/sh

curl -H 'Content-Type: application/json' -XGET "http://localhost:9200/document-store/_search?pretty=true&size=1000" -d'
{
    "query": {
        "exists": {"field": "relevance-judgment"}
    }
}'