#!/bin/sh    

curl -H 'Content-Type: application/json' -XGET "http://localhost:9200/query-store/_search?pretty=true&size=100" -d'
{
    "query": {
        "range": {
            "datetime": {
                "gte": "2018-09-03T17:20:00.000",
                "lte": "2018-09-03T18:40:00.000"
            }
        }
    },
    "sort": {
        "datetime": {
            "order": "asc"
        }
    }
}'