#!/usr/bin/env python3
if __name__ == '__main__':
    
    import json

    path = 'docs/evaluation/lab-experiment-relevance-judgements.json'

    out = open('docs/evaluation/rs2_trec_rel_file', 'w')

    with open(path) as f:
        data = json.load(f)
        for hit in data['hits']['hits']:
            if 'relevance-judgment' in hit['_source']:
                judgment = hit['_source']['relevance-judgment']
                task_id = list(judgment.keys())[0]
                is_relevant = judgment[task_id]['lukas'] + judgment[task_id]['lucas'] + judgment[task_id]['jonathan'] >= 2
                out.write('{} {} {} {:d}\n'.format(task_id, 0, hit['_id'], is_relevant))

        
