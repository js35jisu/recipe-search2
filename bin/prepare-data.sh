#!/bin/sh

# strip first header line from original csv
echo -n "removing csv header.."
tail -n +2 data/epi_r.csv > data/epi_r_headless.csv
echo " done."

# import csv into relational db
echo -n "importing csv data into sqlite db.."
sqlite3 data/epi_r.sqlite < processing/import-csv.sql
echo " done."

# extract json objects from array and store one array element per line
echo -n "transforming documents.."
node processing/transform-documents.js
echo " done."
