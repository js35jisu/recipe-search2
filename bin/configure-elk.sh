
echo "disable index.."

curl -X POST "localhost:9200/document-store/_close"

echo " done."

echo "settings english analyzer.."

curl -X PUT "localhost:9200/document-store/_settings" -H 'Content-Type: application/json' -d'
{
  "analysis" : {
    "analyzer":{
      "content":{
        "type":"english"
      }
    }
  }
}
'

echo " done."

echo "re-opening index.."

curl -X POST "localhost:9200/document-store/_open"

echo " done."

echo "creating query store.."

curl -X PUT "localhost:9200/query-store"

echo " done."
