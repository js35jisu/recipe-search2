import Vue from 'vue'
import Router from 'vue-router'

const routerOptions = [
  {
    path: '/',
    component: 'Home',
    props: (route) => ({
      // parse query param 'eval' to ensure a boolean gets passed to prop 'eval'
      eval: (route.query.eval === 'true')
    })
  }
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/components/${route.component}.vue`)
  }
})

Vue.use(Router)

export default new Router({
  routes,
  mode: 'history'
})
