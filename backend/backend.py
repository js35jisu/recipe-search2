from flask import Flask, jsonify, render_template, request, abort
from config import Config
from flask_cors import CORS
import datetime
import requests
from elasticsearch import Elasticsearch

es = Elasticsearch(
    'localhost:9200',
    use_ssl=False
)

app = Flask(__name__,
            static_folder="../frontend/dist/static",
            template_folder="../frontend/dist")
app.config.from_object(Config)

CORS(app)


@app.route('/search', methods=['GET'])
def getMatchingRecipies():
    """Route for search queries
       parameters passed in the url: 
          q      - query string
          fat    - specifies amount of fat
          sod    - specifies amount of sodium
          prot   - specifies amount of protein
          cal    - specifies calories
       returns json-object containing the query results
    """
    query = request.args.get('q', default='', type=str)
    fat = request.args.get('fat', default=None, type=int)
    cal = request.args.get('cal', default=None, type=int)
    sod = request.args.get('sod', default=None, type=int)
    prot = request.args.get('prot', default=None, type=int)

    logID = trackQuery(query, request.remote_addr, fat, cal, sod, prot)
    results = boolQuery(query, cal, fat, sod, prot)
    results['logID'] = logID
    return jsonify(results)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def renderFrontend(path):
    """Render vue frontend app.

    All unspecified routes are passed to the vue router
    to be handled in our fronted vue app.
    """
    return render_template("index.html")


def trackQuery(query: str, ipaddress: str, fat: int, cal: int, sod: int, prot: int):
    """
    Tracks a query while indexing a new query document. 
    The visitor's ip address and the time the query was transmitted are tracked.
    Returns logID.
    """
    log = {'ipaddress': ipaddress, 'query': query, 'hits': {},
           'datetime': str(datetime.datetime.now().isoformat())}
    if fat is not None:
        log['fat'] = fat
    if cal is not None:
        log['cal'] = cal
    if sod is not None:
        log['sod'] = sod
    if prot is not None:
        log['prot'] = prot
    return es.index(index='query-store', body=log, doc_type='_doc')['_id']


@app.route('/track/hit/<docid>', methods=['PUT'])
def trackHit(docid):
    """
    The hit documents within a query are tracked. 
    Moreover the hit counter of a the hit documents will be incremented.
    Returns a JSON-Object containing the Log-ID.
    """
    logid = request.json['logId']
    log = es.get(index='query-store', id=logid, doc_type='_doc')

    if not docid in log['_source']['hits'].keys():
        log['_source']['hits'][docid] = []

    doc = es.get(index='document-store', id=docid, doc_type='_doc')
    try:
        currentCount = int(doc['_source']['hitCount'])
        doc['_source']['hitCount'] = str(currentCount + 1)
    except KeyError:
        doc['_source']['hitCount'] = '1'
    es.index(index='document-store',
             body=doc['_source'], id=docid, doc_type='_doc')

    return jsonify(es.index(index='query-store', body=log['_source'], doc_type='_doc', id=logid))


@app.route('/relevance-judgement', methods=['POST'])
def relevanceJudgement():
    """
    Route for relevance judgements.
        parameters passed in the URL:
            user    - user who judget the document for relevance
            task    - task in which the document is judged
            docId   - document which is judged
            value   - indicates the relevance of the document judged. Defaults to False.
    """

    # parse input
    user = request.args.get('user', default=None, type=str)
    task = request.args.get('task', default=None, type=str)
    docId = request.args.get('docId', default=None, type=str)
    value = request.args.get('value', default='', type=str)

    # sanitize params
    value = value.lower() == 'true'
    user = user.lower()
    if user is None or task is None or docId is None:
        print('user, task, docId need to be provided')
        abort(400)

    # fetch document
    doc = es.get(index='document-store', id=docId, doc_type='_doc')

    # add and if already present,
    # overwrite the specific per-user per-document and per-task judgement
    if not 'relevance-judgment' in doc['_source'].keys():
        doc['_source']['relevance-judgment'] = {}

    if not task in doc['_source']['relevance-judgment'].keys():
        doc['_source']['relevance-judgment'][task] = {}

    doc['_source']['relevance-judgment'][task][user] = value

    return jsonify(es.index(index='document-store', body=doc['_source'], id=docId, doc_type='_doc'))


@app.route('/track/time/<dwelltime>', methods=['PUT'])
def trackTime(dwelltime: str):
    """
    The dwell time of a hit document is tracked.
    Returns a JSON-Object containing the Log-ID.
    """
    logid = request.json['logId']
    docid = request.json['docId']
    log = es.get(index='query-store', id=logid, doc_type='_doc')
    log['_source']['hits'][docid].append(int(dwelltime))
    return jsonify(es.index(index='query-store', body=log['_source'], doc_type='_doc', id=logid))


@app.route('/autocomplete/<query>')
def get_autocomplete_suggestions(query: str) -> str:
    """Fetch autocomplete suggestions from Elasticsearch.

    Autocompletion queries are determined using only the last token of our query against the term-suggestor-api.
    The resulting term list is sorted by score and frequency lexicographically and then unified,
    before being re-concatenated to the query, jsonified and returned.

    Parameters
    ----------
    query: string (route parameter)
        The query to be autocompleted.
    size: int (query paramter)
        The desired size of the result list.
        Will be normalized to [0, 10].

    Returns
    -------
        A json-array of autocomplete-suggestion queries.

    """

    # parse optional size parameter so that 0 <= size <= 10
    result_size = request.args.get('size', default=5, type=int)
    result_size = min(max(0, result_size), 10)

    # tokenize query and extract last term
    tokens = query.split()
    last_term = tokens[-1]
    prefix_tokens = tokens[:-1]

    # compose suggest query
    suggest_fields = ['title', 'directions', 'ingredients', 'desc']
    suggest_query = {'text': last_term}
    for suggest_field in suggest_fields:
        suggest_query[suggest_field] = {
            'term': {
                'field': suggest_field
            }
        }

    # execute suggest query
    suggest_result = es.search(
        index='document-store', body={'suggest': suggest_query})

    # destructure all suggestion results
    suggest_result = suggest_result['suggest']
    suggestions = []
    for suggest_field, single_result in suggest_result.items():
        for single_partial_result in single_result:
            suggestions = suggestions + \
                single_partial_result['options']

    # sort suggestions on score and frequency: lexicographically and descending
    suggestions.sort(
        key=lambda option: (option['score'], option['freq']),
        reverse=True)

    # remove duplicates preserving order
    suggest_terms = [s['text'] for s in suggestions]

    def unifiy(terms: list) -> list:
        seen = set()
        seen_add = seen.add
        return [t for t in terms if not (t in seen or seen_add(t))]
    unified_suggestions = unifiy(suggest_terms)

    # strip results
    unified_suggestions = unified_suggestions[:result_size]

    # re-join prefix tokens
    concatenated_tokens = [prefix_tokens + [us]
                           for us in unified_suggestions]
    query_suggestions = [' '.join(ct) + ' ' for ct in concatenated_tokens]
    return jsonify([{'value': qs} for qs in query_suggestions])
    
@app.route('/format-result', methods=['GET'])
def createTrecEvalTopFile():
    """ Create a file that is used by trec_eval to compute evaluation.
    
    Four queries that correspond to the four topics that were used to evaluate the effectiveness of this retrieval system are being sent to the index.
    The result sets are formatted to conform the trec_eval_top_file requirements
    and then written to the file 'rs2_trec_top_file' in the docs module.
    """
    
    resultsT1 = boolQuery('quick recipe potatoe onion tomato', None, None, None, None)
    resultsT2 = boolQuery('porridge gochi berry banana high carb', None, None, None, None)
    resultsT3 = boolQuery('lunch low carb less than 500 kcal', 500, None, None, None)
    resultsT4 = boolQuery('low sugar diet ice cream', None, None, None, None)
    
    f = open('docs/evaluation/rs2_trec_top_file', 'w')
    
    for i, hit in enumerate(resultsT1['hits']['hits']):
        f.write('{} {} {} {} {} {}\n'.format(1, 'Q0', hit['_id'], i, hit['_score'], 'STANDARD'))
    
    for i, hit in enumerate(resultsT2['hits']['hits']):
        f.write('{} {} {} {} {} {}\n'.format(2, 'Q0', hit['_id'], i, hit['_score'], 'STANDARD'))
    
    for i, hit in enumerate(resultsT3['hits']['hits']):
        f.write('{} {} {} {} {} {}\n'.format(3, 'Q0', hit['_id'], i, hit['_score'], 'STANDARD'))
    
    for i, hit in enumerate(resultsT4['hits']['hits']):
        f.write('{} {} {} {} {} {}\n'.format(4, 'Q0', hit['_id'], i, hit['_score'], 'STANDARD'))
        
    abort(418)
    


def matchLables(query: str):
    """Use textual fields title, description, directions and categories for keyword search.

    Exclude ingredients as this fields content is redundant with directions and includes many stopwords.
    """
    lables = []
    lables.append({'match': {'title': query}})
    lables.append({'match': {'desc': query}})
    lables.append({'match': {'directions': query}})
    lables.append({'match': {'categories': query}})
    return lables


def matchNutrition(cal: int, fat: int, sod: int, prot: int):
    """
    Specifies calories, fat, sodium and prot of the query. 
    """
    nutritionFacts = []
    if cal is not None:
        nutritionFacts.append(
            {'range': {'calories': {'lte': cal}}})
    if fat is not None:
        nutritionFacts.append(
            {'range': {'fat': {'lte': fat}}})
    if sod is not None:
        nutritionFacts.append(
            {'range': {'sodium': {'lte': sod}}})
    if prot is not None:
        nutritionFacts.append(
            {'range': {'protein': {'lte': prot}}})
    return nutritionFacts


def boolQuery(query: str, cal: int, fat: int, sod: int, prot: int):
    """
    Queries a document that satisfies the query string, calories, fat, sodium and protein.
    Returns the query result. 
    """
    query = {
        'query': {
            'bool': {
                'should': matchLables(query),
                'must': matchNutrition(cal, fat, sod, prot)
            }
        },
        'sort': [{'_score': 'desc'}],  # descending
        'highlight': {
            'fields': {
                'desc': {'type': 'plain'},  # description
                'directions': {'type': 'plain'}
            }
        }
    }
    return es.search(body=query)
