# README

> Data was downloaded from Kaggle: https://www.kaggle.com/hugodarwood/epirecipes/data.

![Image of a Burger and Fries](https://images.unsplash.com/photo-1477617722074-45613a51bf6d?ixlib=rb-0.3.5&s=f6e4190deb1f021cfa65d8c03012d9ac&dpr=1&auto=format&fit=crop&w=1000&q=80&cs=tinysrgb)

## Table of Contents

- [Installation and Running](#installation-and-running)
  - [Quickstart ubuntu](#quickstart-ubuntu)
  - [step-by-step](#step-by-step)
- [Further Information](#further-documentation)
- [Frontend](#frontend)
  - [Evaluation Mode](#evaluation-mode)
- [Backend](#backend)
- [Docs](#docs)
- [trec_eval](#trec_eval)

## Installation and Running

### Quickstart ubuntu

Simply run:

```sh
./run/ubuntu.sh
```

To stop the running services run the following commands respectively:

```sh
# stop flask backend
./bin/stop-flask.sh

# stop elastic search server
./bin/stop-elk.sh
```

### step-by-step

#### Preprocessing and docker startup

```sh
# import csv data into sqlite DB and prepare recipes for bulk insertion
./bin/prepare-data.sh

# if you're running linux, virtual memory needs to be configured
./bin/configure-virtual-mem.sh

# start docker container
./bin/start-elk.sh
```

#### Configure docker and insert documents

```sh
# while the docker is running: fill the elasticsearch instance
./bin/bulk-insert-document-store.sh
# .. and configure it
./bin/configure-elk.sh

# Build frontend and start backend
# install vue and build frontend
cd frontend/
npm install
npm run build

# install flask
cd ../backend/
pipenv install Pipfile

# run flask and serve static frontend (served at localhost:5000)
cd ../
./bin/start-flask.sh
```

## Further Documentation

- [Project-Wiki](https://git.informatik.uni-leipzig.de/js35jisu/recipe-search2/wikis/home)
- [ElasticSearch](https://www.elastic.co/guide/en/elasticsearch/reference/5.0/index.html)
- [ELK-Docker](https://elk-docker.readthedocs.io/)
- docker-compose.yml: [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

### Dependencies

#### Necessary

- [curl](https://curl.haxx.se/)
  - Shipped with most UNIX systems and is contained in git bash.
- [node.js](https://nodejs.org/)
- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [python](https://www.python.org/downloads/)
- [pip](https://pip.pypa.io/en/stable/installing/)
- [pipenv](https://pipenv.readthedocs.io/en/latest/install/)
- You'll need a UNIX environment to run the scripts located in the `bin` directory.
  - On windows use the git bash, which is included with [git](https://git-scm.com/download/win). Be sure to explicitely check _install git bash_ in the installation process.

#### Optional

- [sqlite3](https://sqlite.org/index.html)
- [reveal-md](https://github.com/webpro/reveal-md)
- [jupyter](http://jupyter.org/install)
- [latex](https://www.latex-project.org/get/)

### Directory Structure

- `bin` holds executables, e.g. shell scripts or binaries.
- `data` holds all data sets and derivatives.
- `docker` holds all docker related configuration.
- `frontend` holds Vue-frontend files.
- `log` contains log files from backend and elk stack.
- `docs` holds all kinds of documents that allow for some deeper understanding of project related issues.
- `.env` is a key-value config file for Kibana, Elasticsearch and Flask.
- `processing` holds utility scripts, mostly called from scripts located in `bin`.
- `run` contains convenience scripts for instaling and running the search engine.

## Frontend

**Vue.js frontend app for a recipe centered search engine.**

### Structure

The source code is structured as a multitude of [components](frontend/src/components).

- `Home.vue`
  Wrapper for `Searchbar.vue` and `ResultList.vue`

- `Searchbar.vue`
  Provides an input field for text queries as well as for advanced search parameters (calories, fat, sodium, protein).
  The input is transmitted to the backend as a GET request. Additionaly, the component implements an autocompletion feature which
  works by sending every input state to the backend, which in turn returns suggestions.

- `ResultList.vue`
  Receives the result set of a search query and keeps it in the `resultSet[]` attribute.
  Each result is visually represented by the `ResultSnippet.vue` component. Therefore, the recipe content
  is passed to the representing snippet as a property. A click on a snippet displays all available recipe data as detailed view. The detailed view is an [el-dialog](http://element.eleme.io/#/en-US/component/dialog) that stores its structure inside of the `ResultList.vue` component

- `ResultSnippet.vue`
  Displays some minimal result-recipe information. Shows the title and highlighted parts from the recipe text where the query matched.
  The title serves as a link to the `ResultDetail.vue` representation of the result.

Component structure:

![structure_frontend](docs/graphics/Frontend-Komponentenaufbau.png)

### Evaluation Mode

To enter the evaluation mode for contributing relevance judgements in a lab experiment, add the `eval` parameter to the request url in your browser:

`http://localhost:5000/?eval=true`

Next to the buttons for submitting search requests and expanding the advanced search form, there will appear a login button. Press it to be requested to enter your user handle and a task identifier for the currently evaluated task.

You are then able to judge search results as relevant or irrelevant via radio buttons that are displayed next to each result.

### Further Documentation

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Backend

### Routes

- `localhost:5000/`: serve frontend production build
- `localhost:5000/search?q=<query>&cal=<cal>&fat=<fat>&sod=<sod>&prot=<prot>`: search API
- `localhost:5000/autocomplete/<query>`: term-suggestor-based autocompletion API
- `localhost:5000/track/hit/<docid>`
- `localhost:5000/track/time/<dwelltime>`
- `localhost:5000/format-result`: queries elasticsearch and creates `trec_eval` input from search results

## Docs

### Slides

To get the slideshow working, simply run `reveal-md /path/to/slides.md`

### Report

.tex files can be compiled using your favourite Latex compiler, e.g.:

```sh
cd docs/report
pdflatex report.tex
xdg-open report.pdf
```

### Jupyter Notebook

To view or edit an ipynb-file, simply `cd` into the respective directory and run `jupyter notebook`
Please make sure you have the required modules installed on your system/virtualenv

## trec_eval

Use a simple HTTP-GET-request to the backend route `localhost:5000/format-result` to create `rs2_trec_top_file`.  
Then create the `trec_rs2_rel_file` via

```sh
./bin/create-trec-rel-file.py
```

The files `rs2_trec_top_file` and `rs2_trec_rel_file` are used by [TREC's](https://trec.nist.gov/) standard
tool for retrieval evaluation called [`trec_eval`](https://trec.nist.gov/trec_eval/). The tool can easily be
downloaded and installed via `make` (setting the compiler to clang in the Makefile may help if there are
core dump issues). The ouput of the programm is also provided in the docs directory.
To reproduce the provided output (`rs2_trec_eval_result`) with the currently saved relevance judgements from a lab experiment entered in the [evaluation mode](#evaluation-mode), `cd` into the `trec_eval` installation directory
and execute `./trec_eval /path/to/rs2_trec_rel_file /path/to/rs2_trec_top_file`
