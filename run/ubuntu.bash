#!/bin/bash

# update system
sudo apt-get update && sudo apt-get -y upgrade

# curl
which curl 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing curl.."
    sudo apt-get install -y curl
fi

# docker
sudo which docker 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing docker.."
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    echo "installed docker:"
    docker --version
fi

# docker-compose
which docker-compose 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing docker-compose.."
    sudo curl -L https://github.com/docker/compose/releases/download/1.23.0-rc1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

# python
which python3 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing python.."
    sudo apt-get install -y python3
fi

# add universe packages
echo "temp. adding universe ubuntu package source.."
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
head -n 1 /etc/apt/sources.list.bak | sudo tee /etc/apt/sources.list 2>&1 > /dev/null
tail -n +2  /etc/apt/sources.list.bak | sed 's/restricted/universe/g' | sudo tee -a /etc/apt/sources.list 2>&1 > /dev/null

echo "/etc/apt/sources.list:"
cat /etc/apt/sources.list
sudo apt-get update

# pip
which pip3 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing pip.."
    sudo apt-get install -y python3-pip
fi

echo "restoring sources.list.."
sudo mv /etc/apt/sources.list.bak /etc/apt/sources.list
cat /etc/apt/sources.list

# pipenv
which pipenv 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing pip.."
    pip3 install pipenv --user
fi

# sqlite3
which sqlite3 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing sqlite3.."
    sudo apt-get install -y sqlite3 libsqlite3-dev
fi

# nodejs
which node 2>&1 > /dev/null
if [ ! $? -eq 0 ]
then
    echo "installing nodejs.."
    curl -sL https://deb.nodesource.com/setup_8.x | sudo bash -
    sudo apt-get install -y nodejs
fi

if [ ! -r data/bulk-document-store.jsonl ]
then
    # import csv data into sqlite DB and prepare recipes for bulk insertion
    ./bin/prepare-data.sh
fi

# workaround for docker storage driver bug, see: https://docs.docker.com/storage/storagedriver/vfs-driver/#configure-docker-with-the-vfs-storage-driver
echo "set vfs docker storage driver.."
sudo systemctl stop docker
echo '{
  "storage-driver": "vfs"
}' | sudo tee /etc/docker/daemon.json
sudo systemctl start docker

# configure virtual mem
./bin/configure-virtual-mem.sh

# start docker container
if [ ! $(curl -sSf localhost:9200 > /dev/null 2>/dev/null) ]
then
    sudo ./bin/start-elk.sh 2>>log/elk.err 1>>log/elk.log &
    
    # wait until elasticsearch is reachable
    echo "waiting for elk docker to spin up.."
    UP=1
    while [ ! $UP -eq 0 ]
    do
	curl -sSf localhost:9200 2>/dev/null > /dev/null
	UP=$?
        sleep 2
        echo -n "."
    done
fi

# bulk insert documents
if [ ! -e data/documents.inserted ]
then
    ./bin/bulk-insert-document-store.sh
    touch data/documents.inserted
fi

./bin/configure-elk.sh 2>&1 >> /dev/null

# install frontend dependencies
cd frontend
npm install

# build frontend
npm run build

# install backend dependencies
cd ../backend
pipenv install

# start backend
cd ..
./bin/start-flask.sh 2>>log/backend.err 1>>log/backend.log &

sleep 2

# access search engine
xdg-open http://localhost:5000
