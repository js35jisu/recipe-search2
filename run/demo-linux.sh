# start docker container
if [ ! $(curl -sSf localhost:9200 > /dev/null 2>/dev/null) ]
then
    sudo ./bin/start-elk.sh 2>>log/elk.err 1>>log/elk.log &
    
    # wait until elasticsearch is reachable
    echo "waiting for elk docker to spin up.."
    UP=1
    while [ ! $UP -eq 0 ]
    do
	curl -sSf localhost:9200 2>/dev/null > /dev/null
	UP=$?
        sleep 2
        echo -n "."
    done
fi

# bulk insert documents
if [ ! -e data/documents.inserted ]
then
    ./bin/bulk-insert-document-store.sh
    touch data/documents.inserted
fi

./bin/configure-elk.sh 2>&1 >> /dev/null

# install frontend dependencies
cd frontend
npm install

# build frontend
npm run build

# install backend dependencies
cd ../backend
pipenv install

# start backend
cd ..
./bin/start-flask.sh 2>>log/backend.err 1>>log/backend.log &

sleep 2

# access search engine
xdg-open http://localhost:5000

