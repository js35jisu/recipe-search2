set term png
set output "rs2_pre_rec_graph.png"
set title "Precision/Recall [Recipe Search 2]"
set ylabel "Precision"
set xlabel "Recall"
set xrange [0:1]
set yrange [0:1]
set xtics 0, .2, 1
set ytics 0, .2, 1

plot 'pre_rec_data.dat' title "Run 1" with lines