# Adhoc Retrieval Protocoll 03.09.2018

> Protocoll of the evaluation experiment.

---

## Jonathan

- Start: 17:21 h
- End: 17:50 h

### Notes

- exact document duplicates in our corpus
- missing description still gets a headline rendered
- "low carb" query matches "low" and "carb"
  - possible solutions are phrase queries and phrase detection

---

## Lukas

- Start: 17:53 h
- End: 18:08 h

### Notes

- no recipes with potatos (task 1)
- only one porridge recipe (task 2)
- "500° F" matches the "500" in "500 kcal" (task 3)
- no low sugar or zero sugar recipes (task 4)

---

## Lucas

- Start: 18:20 h
- End: 18:40 h

### Notes

(None)
