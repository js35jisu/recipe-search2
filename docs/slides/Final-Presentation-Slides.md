---
title: Recipe-Search2 - Intermediate Status
revealOptions:
  transition: "fade"
  slideNumber: true
---

<!-- .slide: data-background="./burger.jpg" -->

# Recipe-Search 2

---

## Agenda

<!-- Jonathan -->

- **Datensatz** <!-- Beispiel json -->
  <!-- Nico -->
- Retrieval-Modell <!-- Okapi BM25 Abwandlung, Preprocessing Pipeline -->
  <!-- Lukas -->
- Technologie-Stack <!-- Abbildung -->
- Tracking <!-- Beispiel Tracking Example, requests -->
  <!-- Lucas -->
- Lab-Experiment <!-- Tasks, Zahlen, Bild 9. Etage-->
- Demo

---

### example-recipe.json (1)

```json
{
  "title": "Veal Burgers Stuffed with Mozzarella Cheese ",
  "directions": [
    "Mix cheese and green onions in medium bowl. /*...*/",
    "Spread mayonnaise mixture over buns. Place /*...*/"
  ],
  "desc": null,
  "categories": [
    "Sandwich",
    "Beef",
    "Cheese",
    /*...*/
  ],
  /*...*/
```

---

### example-recipe.json (2)

```json
  /*...*/
  "ingredients": [
    "1 cup shredded mozzarella cheese",
    "1/4 cup finely chopped green onions",
    "1 1/4 pounds ground veal",
      /*...*/
  ],
  "calories": 904,  // kCal
  "fat": 70,  // g
  "protein": 38,  // g
  "sodium": 1413,  // mg
  "rating": 4.375,
  "date": "2004-08-20T04:00:00.000Z",
}
```

---

### Verteilung der Textfelder

|             | präsent | (%)  |
| ----------- | ------: | ---- |
| desc        |   13495 | 67.0 |
| categories  |   20111 | 99.9 |
| date        |   20111 | 99.9 |
| directions  |   20111 | 99.9 |
| ingredients |   20111 | 99.9 |
| title       |   20111 | 99.9 |
| all         |   20111 | 99.9 |

---

### Verteilung der numerischen Felder

|          | präsent | (%)  |
| -------- | ------: | ---- |
| fat      |   15908 | 79.0 |
| protein  |   15929 | 79.1 |
| sodium   |   15974 | 79.4 |
| calories |   15976 | 79.4 |
| rating   |   20100 | 99.9 |
| alle     |   15903 | 79.0 |

---

## Agenda

<!-- Jonathan -->

- ~~Datensatz~~ <!-- Beispiel json -->
  <!-- Nico -->
- **Retrieval-Modell** <!-- Okapi BM25 Abwandlung, Preprocessing Pipeline -->
  <!-- Lukas -->
- Technologie-Stack <!-- Abbildung -->
  <!-- Lucas -->
- Lab-Experiment <!-- Tasks, Zahlen, Bild 9. Etage-->
- Demo

---

### Retrieval Modell

![](preprocessingPipeline.png)

---

### Retrieval Modell

- Okapi BM25 Ranking-Funktion
- Berücksichtigung von Term-Frequenz und der Dokumentenlänge
- hartes Matchen der Nährwerte

---

## Agenda

<!-- Jonathan -->

- ~~Datensatz~~ <!-- Beispiel json -->
  <!-- Nico -->
- ~~Retrieval-Modell~~ <!-- Okapi BM25 Abwandlung, Preprocessing Pipeline -->
  <!-- Lukas -->
- **Technologie-Stack** <!-- Abbildung -->
- Tracking <!-- Beispiel Tracking Example, requests -->
  <!-- Lucas -->
- Lab-Experiment <!-- Tasks, Zahlen, Bild 9. Etage-->
- Demo

---

### Tech Stack

![](techstack.png)

---

<img src="architecure_search_engine.png" height="640"/> <!-- TODO fix height for beamer -->

---

## Agenda

<!-- Jonathan -->

- ~~Datensatz~~ <!-- Beispiel json -->
  <!-- Nico -->
- ~~Retrieval-Modell~~ <!-- Okapi BM25 Abwandlung, Preprocessing Pipeline -->
  <!-- Lukas -->
- ~~Technologie-Stack~~ <!-- Abbildung -->
- **Tracking** <!-- Beispiel Tracking Example, requests -->
  <!-- Lucas -->
- Lab-Experiment <!-- Tasks, Zahlen, Bild 9. Etage-->
- Demo

---

### User Interaction Logging

```json
{
  "ipaddress": "127.0.0.1",
  "query": "lunch low carb less than 500 kcal", // text query
  "hits": {
    "772": [
      // document id
      32200 // dwell time (milliseconds)
    ],
    "2567": [30800]
  },
  "datetime": "2018-09-03T17:39:29.576264",
  "cal": 500 // advanced search parameter for calories
  /*[...]*/
}
```

---

## Agenda

<!-- Jonathan -->

- ~~Datensatz~~ <!-- Beispiel json -->
  <!-- Nico -->
- ~~Retrieval-Modell~~ <!-- Okapi BM25 Abwandlung, Preprocessing Pipeline -->
  <!-- Lukas -->
- ~~Technologie-Stack~~ <!-- Abbildung -->
- ~~Tracking~~ <!-- Beispiel Tracking Example, requests -->
  <!-- Lucas -->
- **Lab-Experiment** <!-- Tasks, Zahlen, Bild 9. Etage-->
- Demo

---

## Lab Experiment

---

### Durchführung

- Es werden vier Retrieval-Tasks formuliert
- Mitglieder der Gruppe bewerten über ein Interface die Ergebnisse der Suchmaschine zu den Retrieval-Tasks
- Aus den Bewertungen werden die Metriken _Recall_ und _Precision_ ermittelt

---

### (1) _quick recipe potatoe onion tomato_

- As a broke student I would like to prepare a dish with potatoes, onions and tomatoes in under 20 minutes.

---

### (2) _porridge gochi berry banana high carb_

- As a vegan I would like to prepare a porridge including gochi berries and bananas to match high carb diet.

---

### (3) _lunch low carb less than 500 kcal_

- As a person tracking nutrition facts I would like to prepare lunch with less than 500kcal matching low carb diet.

---

### (4) _low sugar diet ice cream_

- As a diabetes sufferer I want to prepare a low sugar ice cream as a dessert.

---

## Ergebnisse

---

![img](rs2_pre_rec_graph.png)

---

- könnte besser sein
- sehr spezifische information needs bei sehr generischer Anfrageverarbeitung
- wiederholtes Suchen mit verfeinerten Anfragen wird nicht abgebildet

---

## Ausblick

---

- Domain-spezifische Phrasenerkennung (z.B. "low carb", "high protein", ...)
- Übersetzung von solchen Phrasen in implizite erweiterte Suchparameter
- Unscharfe Suche für exakte Nährwertangaben

---

- Bereichsuche für Nährwertangaben (obere und untere statt nur obere Schranken)
- Sortierfunktion für Sortierung nach Nährwertangaben im Web-Interface
- Zutatenfilter per Kategorie (vegan, glutenfrei, ...)

---

# DEMO
