---
title: Recipe-Search2 - Prototype
revealOptions:
    transition: "fade"
    slideNumber: true
---

<!-- .slide: data-background="./lightning.jpg" -->
# Recipe-Search 2

---

# Agenda

* __Domain__
* Technology-Stack
* Prototyp 

---

## Domain

* Information-Need:  
  Zubereitungsanleitung von Gerichten
* Suchansätze:
  * Zutaten
  * Rezeptbeschreibung
  * Nährwerte

---

## Use Case 1: Zutaten

> As a broke student I would like to prepare a dish with potatoes, onions and tomatoes in under 20 minutes. 

|query|
|---|
|*"quick recipe potatoe onion tomato"*|

---

## Use Case 2: Rezeptbeschreibung

> As a vegan I would like to prepare a porridge including gochi berries and bananas to match high carb diet.

|query|
|---|
|*"porridge gochi berry banana high carb"*|

---

## Use Case 3: Nährwerte

> As a person tracking nutritional facts I would like to prepare lunch with less than 500cal and protein less than 15 g matching low carb diet.

|query (Advanced Search)|
|---|
|*"low protein low carb less than 500 cal"*|
---

## Datensatz

* Datensatz: 20.000 + Rezepte in Englisch von [kaggle](https://www.kaggle.com/hugodarwood/epirecipes)
* Format: CSV und JSON
* Daten enthalten:
  * Titel
  * Beschreibung
  * Anleitung
  * Nährwertdaten

---

## Datensatz Beispiel

```json
{
    "directions": [...],
    "fat": 70,
    "date": "2004-08-20T04:00:00.000Z",
    "categories": [...],
    "calories": 904,
    "desc": null,
    "protein": 38,
    "rating": 4.375,
    "title": "Veal Burgers Stuffed with Mozzarella Cheese ",
    "ingredients": [...],
    "sodium": 1413
},
```

---

# Agenda

* ~~Domain~~
* __Technology-Stack__
* Prototyp 

---

## Architekur

* Document Store, Indexing: Elastic Search, Kibana
* Backend: Flask
* Frontend: Vue.js

---

## Architektur

![ir_dev1](./arch.png)

---

# Agenda

* ~~Domain~~
* ~~Technology-Stack~~
* __Prototyp__ 

---

<!-- .slide: data-background="./burger.jpg" -->
# [Prototyp](http://localhost:8080)



