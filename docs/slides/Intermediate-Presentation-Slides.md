---
title: Recipe-Search2 - Intermediate Status
revealOptions:
    transition: "fade"
    slideNumber: true
---

<!-- .slide: data-background="./burger.jpg" -->

# Recipe-Search 2

---

## Agenda

- <!-- .element: class="fragment" data-fragment-index="1" -->
  **Autocompletion** <!-- .element: class="fragment" data-fragment-index="1" -->
- Data Analysis <!-- .element: class="fragment" data-fragment-index="2" -->
- User-Interaction-Logging <!-- .element: class="fragment" data-fragment-index="3" -->
- Advanced Search <!-- .element: class="fragment" data-fragment-index="4" -->

NOTE: Upgrades seit dem 29.05.

---

### Autocompletion

- <!-- .element: class="fragment" data-fragment-index="1" --> [Elastic Search Suggester-API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-term.html) <!-- .element: class="fragment" data-fragment-index="1" -->
  - based on <!-- .element: class="fragment" data-fragment-index="2" --> [_Damerau-Levenshtein distance_](https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance) <!-- .element: class="fragment" data-fragment-index="2" -->
  - suggests k nearest neighbours for a given term <!-- .element: class="fragment" data-fragment-index="3" -->
- tokenize query and provide alternatives only for its last token <!-- .element: class="fragment" data-fragment-index="4" -->

NOTE: - Phrasen-, Completion und Context-Suggestors erwarten MANUELLE (gewichtete) Eingabe von Vorschlägen <br> - Term Suggestor ist zwar automatisch, verhält sich aber eher wie Autocorrect <br> - Term-Suggestor-API ist Teil der Elastic-Search-API <br> - Editier-Distanz <br> - optimiert für effiziente Berechnung für Elastic-/Lucene-Index <br> - Ausblick: Probabilistisches N-Gram-Sprachmodell als zweiter Index

---

![img](Autocomplete.png)

---

![img](Autocomplete-2.png)

---

## Agenda

- ~~Autocompletion~~
- **Data Analysis**
- User-Interaction-Logging
- Advanced Search

---

## Data Analysis: Agenda

- presence <!-- .element: class="fragment" data-fragment-index="1" -->
- completeness <!-- .element: class="fragment" data-fragment-index="2" -->
- individual distributions <!-- .element: class="fragment" data-fragment-index="3" -->
- identifying outliers <!-- .element: class="fragment" data-fragment-index="4" -->

---

### Presence (textual fields)

| field       | presence (%) |
| ----------- | ------------ |
| categories  | 100          |
| directions  | 100          |
| ingredients | 100          |
| title       | 100          |
| description | 67.1         |

---

### Remarks

- <!-- .element: class="fragment" data-fragment-index="1" --> _description_ is treated as optional, additional text input
- <!-- .element: class="fragment" data-fragment-index="2" --> _ingredients_ makes for a bad textual field <!-- .element: class="fragment" data-fragment-index="2" -->
  - <!-- .element: class="fragment" data-fragment-index="3" --> contains a lot of highly functional words (e.g. weight measures) <!-- .element: class="fragment" data-fragment-index="3" -->
  - <!-- .element: class="fragment" data-fragment-index="4" --> is always cited in the _directions_ field

---

### Presence (nutritional fields)

| field                             | unpresent | (%)  | present | (%)  |
| --------------------------------- | --------- | ---- | ------- | ---- |
| rating                            | 11        | 0.0  | 20100   | 100  |
| fat                               | 4203      | 20.8 | 15908   | 79.1 |
| protein                           | 4182      | 20.8 | 15929   | 79.2 |
| sodium                            | 4137      | 20.6 | 15974   | 79.4 |
| calories                          | 4135      | 20.6 | 15976   | 79.4 |
| fat & protein & sodium & calories | 4135      | 20.6 | 15976   | 79.4 |

---

### Remarks

- <!-- .element: class="fragment" data-fragment-index="1" --> presence of nutritional values can be precisely classified

---

### Distributions

- <!-- .element: class="fragment" data-fragment-index="1" --> calories
- <!-- .element: class="fragment" data-fragment-index="2" --> fat
- <!-- .element: class="fragment" data-fragment-index="3" --> protein
- <!-- .element: class="fragment" data-fragment-index="4" --> sodium
- <!-- .element: class="fragment" data-fragment-index="5" --> rating

NOTE: Rating hier der crawlte Wert; User ratings vom Datenursprung

---

### Distribution - Calories

<iframe src="http://0.0.0.0:5601/app/kibana#/visualize/edit/d9e2a2f0-7ded-11e8-b0dc-bfbd29b08368?embed=true&_g=(refreshInterval%3A('%24%24hashKey'%3A'object%3A1416'%2Cdisplay%3A'5%20seconds'%2Cpause%3A!f%2Csection%3A1%2Cvalue%3A5000)%2Ctime%3A(from%3Anow%2Fd%2Cmode%3Aquick%2Cto%3Anow%2Fd))" height="600" width="800"></iframe>

---

### Distribution - Fat

<iframe src="http://0.0.0.0:5601/app/kibana#/visualize/edit/99bae150-7dee-11e8-b0dc-bfbd29b08368?embed=true&_g=(refreshInterval%3A('%24%24hashKey'%3A'object%3A1416'%2Cdisplay%3A'5%20seconds'%2Cpause%3A!f%2Csection%3A1%2Cvalue%3A5000)%2Ctime%3A(from%3Anow%2Fd%2Cmode%3Aquick%2Cto%3Anow%2Fd))" height="600" width="800"></iframe>

---

### Distribution - Protein

<iframe src="http://0.0.0.0:5601/app/kibana#/visualize/edit/6fe69bc0-7def-11e8-b0dc-bfbd29b08368?embed=true&_g=(refreshInterval%3A('%24%24hashKey'%3A'object%3A1416'%2Cdisplay%3A'5%20seconds'%2Cpause%3A!f%2Csection%3A1%2Cvalue%3A5000)%2Ctime%3A(from%3Anow%2Fd%2Cmode%3Aquick%2Cto%3Anow%2Fd))" height="600" width="800"></iframe>

---

### Distribution - Sodium

<iframe src="http://0.0.0.0:5601/app/kibana#/visualize/edit/db17b5a0-7def-11e8-b0dc-bfbd29b08368?embed=true&_g=(refreshInterval%3A('%24%24hashKey'%3A'object%3A1416'%2Cdisplay%3A'5%20seconds'%2Cpause%3A!f%2Csection%3A1%2Cvalue%3A5000)%2Ctime%3A(from%3Anow%2Fd%2Cmode%3Aquick%2Cto%3Anow%2Fd))" height="600" width="800"></iframe>

---

### Distribution - Rating

<iframe src="http://0.0.0.0:5601/app/kibana#/visualize/edit/cdae95e0-7e04-11e8-b0dc-bfbd29b08368?embed=true&_g=(refreshInterval%3A('%24%24hashKey'%3A'object%3A1416'%2Cdisplay%3A'5%20seconds'%2Cpause%3A!f%2Csection%3A1%2Cvalue%3A5000)%2Ctime%3A(from%3Anow%2Fd%2Cmode%3Aquick%2Cto%3Anow%2Fd))" height="600" width="800"></iframe>

NOTE: - überall Long-Tail <br> - Utopisch Hohe Wert durch Crawling-Fehler <br> - **Cappen** um Verteilung von realistischen Werten besser betrachten zu können.

---

### Remarks

- <!-- .element: class="fragment" data-fragment-index="1" --> _rating_ is a crawled value

---

## Agenda

- ~~Autocompletion~~
- ~~Data Analysis~~
- **User-Interaction-Logging**
- Advanced Search

---

### User-Interaction-Logging

1.  <!-- .element: class="fragment" data-fragment-index="1" --> queries
2.  <!-- .element: class="fragment" data-fragment-index="2" --> document hits
3.  <!-- .element: class="fragment" data-fragment-index="3" --> dwell times

NOTE: - dwell time zwischen Öffnen und Schließen eines Dokumentes <br> - nicht explizit geschlossene Dokumente (Rechner verlassen, Browser geschlossen) werden nicht dwell-Time-getrackt.

---

![](User-Interaction-Logging.png)

---

![](User-Interaction-Logging-2.png)

---

![](Log-Entry.png)

---

## Agenda

- ~~Autocompletion~~
- ~~Data Analysis~~
- ~~User-Interaction-Logging~~
- **Advanced Search**

---

### Advanced Search

- <!-- .element: class="fragment" data-fragment-index="1" --> input numeric values in advanced search form
- <!-- .element: class="fragment" data-fragment-index="2" --> interpreted as maximal values
- <!-- .element: class="fragment" data-fragment-index="3" --> hard evaluation
- <!-- .element: class="fragment" data-fragment-index="4" --> unpresent nutrional values are treated as a mismatch

NOTE: - Nährwert-Mismatch -> irrelevant

---

![img](Advanced-Search-1.png)

---

![img](Advanced-Search-2.png)

---

![img](Advanced-Search-3.png)

---

## Agenda

- ~~Autocompletion~~
- ~~Data Analysis~~
- ~~User-Interaction-Logging~~
- ~~Advanced Search~~

---

## Outlook

- <!-- .element: class="fragment" data-fragment-index="1" --> minor frontend fixes
- <!-- .element: class="fragment" data-fragment-index="2" --> track advanced search parameters
- <!-- .element: class="fragment" data-fragment-index="3" --> extend autocompletion with word n-gram models
- <!-- .element: class="fragment" data-fragment-index="4" --> handle outliers
  - <!-- .element: class="fragment" data-fragment-index="5" --> crawling mistakes
  - <!-- .element: class="fragment" data-fragment-index="6" --> wrong data in the first place
- <!-- .element: class="fragment" data-fragment-index="7" --> evaluation & report

NOTE: - Autocompletion: nice-to-have, eher Sprachmodellierung <br>

---

# Q & A
