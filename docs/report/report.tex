% !TeX spellcheck = de_DE
\documentclass[a4paper,11pt,oneside,titlepage]{article}
\usepackage[a4paper]{geometry}
\geometry{a4paper,left=20mm, right=25mm, top=20mm, bottom=30mm}
\usepackage[ngerman]{babel}
\usepackage{hyperref}
\usepackage[toc]{glossaries}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{dirtree}
\usepackage[]{listings}
\usepackage{xcolor}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{booktabs}
\usepackage{nameref}
\usepackage{minibox}
\usepackage{wrapfig}

% custom colors

\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

% general formatting

\pagestyle{fancy}

\lhead{\nouppercase{\leftmark}}
\chead{}
\rhead{\nouppercase{\rightmark}}
\lfoot{\today}
\cfoot{}
\rfoot{\thepage}
\title{Information Retrieval \\ Entwicklung einer Suchmaschine \\ Projektbericht}
\author{Lukas Gehrke \\ Nico Reichenbach \\ Lucas Schons \\ Jonathan Schlue}
\date{Sommersemester 2018}

% general listing setup

\lstset{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize * 0.7,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=bash,                   % the language of the code
  morekeywords={*,...,sudo,node,npm,apt},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                     % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}


% json listing setup

\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numbers=left,
    numberstyle=\scriptsize,
    stepnumber=1,
    numbersep=8pt,
    showstringspaces=false,
    breaklines=false,
    frame=none,
    backgroundcolor=\color{background},
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}

% begin document

\begin{document}

\maketitle

\tableofcontents

\pagebreak

\section{Domäne}

\subsection{Motivation \label{sec:motivation}}

Mahlzeiten und deren Zubereitung gehören zu den Grundbedürfnissen des Menschen. Trends wie gesunde und bewusste wie ökologische Ernährung,
aber auch Unverträglichkeiten oder vegane Ernährung verkomplizieren eben diese Zubereitung und es entsteht ein Informationsbedürfnis nach
passenden Kochrezepten. In diesem Zusammenhang erlangen Nährwerte und individuelle Inhaltsstoffe eine besondere Bedeutung als Kriterien
für die Auswahl solch passender Rezepte.

Wir wollen dieses Informationsbedürfnis nach Kochrezepten, die zu den individuellen Anforderungen an Inhaltsstoffe und Nährwerte passen, im Rahmen
unseres Projektes mit einer umfassenden Rezeptsuchmaschine addressieren. Besonderes Augenmerk liegt dabei darauf, eine klassische
Schlüsselwortsuche mit skalaren Suchparametern im Rahmen einer erweiterten Suchmaske zu verknüpfen. Unser Ziel ist es, die für ein konkretes Informationsbedürfnis
relevanten Rezepte performant und übersichtlich in einer Webanwendung darzustellen. Nicht zuletzt wollen wir uns auf State-of-the-Art-Technologien
stützen, um dieses Ziel effizient zu realisieren.

\subsection{Daten}

Als Datengrundlage verwenden wir die Rezeptdatenbank \textit{Epicurious - Recipes with Rating and Nutrition}\footnote{https://www.kaggle.com/hugodarwood/epirecipes}.
Es handelt sich dabei um $20130$ strukturierte Dokumente, die je ein Rezept durch Titel, Anleitungs- und Beschreibungstexte, Datum, Nährwertangaben, einer Bewertung und
einer Menge von Kategorien näher beschreiben.

\subsection{Analyse}

Der Datensatz liegt sowohl als \textit{CSV}, als auch in \textit{JSON} vor. Bei den Daten im \textit{CSV}-Format handelt es sich lediglich um eine reduzierte und
normalisierte Untermenge der Daten in \textit{JSON}, weshalb wir unsere Arbeit ausschließlich mit der vollständigeren Version in \textit{JSON} fortsetzen. Genauer
handelt es sich dabei um ein \textit{JSON}-Array von \textit{JSON}-Objekten. Ein solches Objekt hat die im Listing in Abbildung \ref{lst:example-recipe} dargestellte Struktur.

\begin{figure}[htb]
	\centering
	\lstinputlisting[language=json,firstnumber=1]{recipe.json}
	\caption{Ein typisches Dokument}
	\label{lst:example-recipe}
\end{figure}

\subsubsection{Textfelder}

Mit Textfeldern sind alle Felder gemeint, die entweder Zeichenketten oder Listen von Zeichenketten beinhalten.
Zunächst können wir beobachten, dass beinahe alle diese Felder bis auf die Beschreibung (desc) in (fast) jedem Dokument gesetzt sind, d.h. keine leere Zeichenkette und nicht \texttt{null} (siehe Abbildung \ref{tbl:presence} (a)).
Somit sind bis auf nicht sinnvoll gesetzte oder fast leere Zeichenketten so gut wie alle Dokumente im Datensatz mindestens für die textuelle Suche brauchbar.

Die Felder Anweisungen (directions), Zutaten (ingredients) und Titel (title) sind entsprechend ihrer Bezeichnung befüllt. Im Feld Datum (date) steht ein Datum-String.
Möglicherweise handelt es sich dabei um das Einstelldatum des Rezepts. An dieser Stelle haben wir nicht weiter recherchiert, da wir dieses Feld für die Befriedigung des Informationsbedürfnisses als irrelevant betrachten (siehe \ref{sec:motivation} \nameref{sec:motivation}).

In den einzelnen Zeichenketten im Feld Kategorien (categories) befinden sich oft Sonderzeichen und \textit{UTF8}-Kodierungsfehler. Zudem doppeln sich viele Kategorien oder übeschneiden sich thematisch.
Möglicherweise handelt es sich dabei um User-Taggings der Nutzer der Plattform, von der unser Datensatz ursprünglich gecrawlt wurde.

\begin{figure}[htbp]
	\centering
	\subfloat[Textfelder]{
		\begin{tabular}{lrr}
			\toprule
			{}          & präsent & (\%) \\
			\midrule
			desc        & 13495   & 67.0 \\
			categories  & 20111   & 99.9 \\
			date        & 20111   & 99.9 \\
			directions  & 20111   & 99.9 \\
			ingredients & 20111   & 99.9 \\
			title       & 20111   & 99.9 \\
			\hline
			alle        & 20111   & 99.9 \\
			\bottomrule
		\end{tabular}
	}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\vspace{0mm}
	\subfloat[Numerische Felder]{
		\begin{tabular}{lrr}
			\toprule
			{}       & präsent & (\%) \\
			\midrule
			fat      & 15908   & 79.0 \\
			protein  & 15929   & 79.1 \\
			sodium   & 15974   & 79.4 \\
			calories & 15976   & 79.4 \\
			rating   & 20100   & 99.9 \\
			{}       & {}      & {}   \\
			\hline
			alle     & 15903   & 79.0 \\
			\bottomrule
		\end{tabular}
	}
	\caption{Vollständigkeit der einzelnen Felder}
	\label{tbl:presence}
\end{figure}

\pagebreak

\subsubsection{Numerische Felder}

In Abbildung \ref{tbl:presence} (b) wird augenscheinlich, dass es unter den numerischen Feldern Unvollständigkeiten gibt.
Zusammengenommen sind aber immer noch $79 \%$ der Datensätze vollständig mit numerischen Werten besetzt.

\begin{figure}[htbp]
	\centering

	\subfloat[Kalorien]{
		\includegraphics[width=70mm]{charts/calories-filtered.png}
	}
	\subfloat[Fett]{
		\includegraphics[width=70mm]{charts/fat-filtered.png}
	}

	\hspace{0mm}

	\subfloat[Protein]{
		\includegraphics[width=70mm]{charts/protein-filtered.png}
	}
	\subfloat[Natrium]{
		\includegraphics[width=70mm]{charts/sodium-filtered.png}
	}

	\hspace{0mm}

	\subfloat[Rating]{
		\includegraphics[width=140mm]{charts/rating.png}
	}

	\caption{Verteilungen numerischer Felder}
	\label{fig:distributions}
\end{figure}

Nun wollen wir die Verteilungen bzw. Histogramme der einzelnen numerischen Felder in Abbildung \ref{fig:distributions} betrachten.
Auf der X-Achse sehen wir stets gleich große, aneinander angrenzende und disjunkte Wertebereiche, sogenannte Bins.
Auf der Y-Achse wird für jeden Bin die Anzahl der Dokumente abgetragen, für die der entsprechende numerische Wert in diesen Bin fällt.

Für alle Nährwertangaben (Abbildung \ref{fig:distributions}, Graphen (a) bis (d)) lässt sich beobachten, dass die Häufigkeit der in einen Bin fallenden Werte ungefähr antiproportional zum Zentroidwert dieses Bins ist.
An dieser Stelle sei angemerkt, dass es einige wenige extreme Ausreißer unter jeder der Nährwertverteilungen gibt.
Gründe für deren Vorkommen können unter anderem Crawling-Fehler, Einheiten-Fehler oder schlicht Fehleingaben von Nutzern sein, die die Rezepte auf der Quellwebsite ursprünglich eingetragen haben.

Die Verteilung der Bewertungen (Rating, Abbildung \ref{fig:distributions}, Graph (e)) hat lokale Maxima bei $0$ und $4.25$.
Grundlage für diese Bewertung sind Reviews mit typischen 5-Sterne-Bewertungen. Aus der dünnen und ungleichen Verteilung sowie aus den kurzen Mantissen, die typisch für Mittelwerte von ganzen Zahlen mit geringer Varianz sind, schließen wir, dass es insgesamt zu wenige solcher Reviews gegeben haben muss und die Bewertungen durch Bildung des arithmetischen Mittels aggregiert wurden.
Deswegen halten wir die Bewertungen für nicht besonders aussagekräftig und verwenden sie an keiner Stelle für das Ranken von Suchergebnissen.

\pagebreak

\section{Architektur}

\begin{figure}[htbp]
	\centering

	\includegraphics[width=\textwidth]{graphics/architecure_search_engine}

	\caption{Architektur}
	\label{fig:architecure_search_engine}
\end{figure}

\subsection{Technologie-Stack}

Als Technologie Stack kommt in dem Projekt eine Architektur bestehend aus einem \textit{Vue.js}\footnote{https://vuejs.org/}-Frontend, einem \textit{Flask}\footnote{http://flask.pocoo.org/}-Backend und einer \textit{Docker}\footnote{https://www.docker.com/}-basierten \textit{Elasticsearch-Kibana}\footnote{https://www.elastic.co}-Instanz zum Einsatz.

Mit dem Komponenten-basierten \textit{JavaScript}-Web-Framework \textit{Vue.js} und der kompatiblen Komponentenbibliothek \textit{element.ui}\footnote{https://element.eleme.io} haben wir eine Suchmaske für Queries und zusätzliche Suchparameter im Rahmen der erweiterten Suche implementiert.
Ferner stellt es die Suchergebnisse als Liste von Ergebnis-Vorschauen bzw. ein ausgewähltes Suchergebnis in einer Detailansicht dar.
Des Weiteren bietet das Frontend einen Evaluationsmodus, in dem es für Aquise von \textit{Relevance-Judgments} verwendet werden kann.

Das Backend verwendet das Python-Framework \textit{Flask} und implementiert eine API, die als Proxy zwischen dem Frontend und \textit{Elasticsearch} agiert. Es verfügt über verschiedene Routen, über die es vom Frontend mittels asynchroner HTTP-Requests angesteuert wird.

\textit{Elasticsearch} vereint den typischen \textit{Inverted Index} und den \textit{Document-Store} in einem sogenannten \textit{Elasticsearch}-Index, der sowohl die Dokumente als auch die Dokument-Repäsentationen beinhaltet.
\textit{Kibana} ist das zugehörige Administrationstool, das wir für Aufgaben der Daten- und Log-Analyse verwenden.

\subsection{Retrieval-Modell}

Nach dem Preprocessing (siehe Abbildung \ref{fig:preprocessingPipeline}) werden die Dokumente in einem invertierten Index gespeichert und durch ihre Indexterme repräsentiert. Als Retrievalmodell wird eine optimierte Variante des Okapi-BM25-Modells\footnote{https://www.elastic.co/guide/en/elasticsearch/guide/current/pluggable-similarites.html} verwendet, das die indizierten Dokumente mit der BM25-Relevanzfunktion gewichtet. Diese berücksichtigt neben der Termfrequenz der Query-Terme auch die Länge der zu gewichtenden Dokumente und kann außerdem durch zwei frei wählbare Parameter angepasst werden.
\begin{figure}[htbp]
	\centering

	\includegraphics[width=\textwidth]{graphics/preprocessingPipeline}

	\caption{Preprocessing-Pipeline}
	\label{fig:preprocessingPipeline}
\end{figure}


\subsection{Web-Interface}

Das Vue.js-Frontend dient einerseits als Eingabemaske für Suchanfragen und andererseits als Darstellungsmedium für die Suchergebnisse.
Es ist aus mehreren Komponenten aufgebaut, die in Parent-Child Beziehungen zueinander stehen.

\textit{Home.vue} funktioniert als Wrapper um \textit{Searchbar.vue} und \textit{ResultList.vue}, die beiden Hauptkomponenten.
\textit{Searchbar.vue} beinhaltet eine Suchleiste und einen Suchbutton, sowie einen weiteren Button, der das Menü für die erweiterte Suche öffnet. Darin können \textit{Kalorien}, \textit{Protein}, \textit{Natrium} und \textit{Fett} für die Suche durch eine obere Schranke eingegrenzt werden.
Die Suchleiste kommuniziert über Events mit der Komponente \textit{ResultList.vue}. Diese hält ein Liste von Child-Komponenten \textit{ResultSnippet.vue}, eine Kompaktdarstellung eines Rezeptes.
Außerdem wird beim Klick auf ein Snippet die Detailansicht eines Rezeptes aktiviert, ein sog. 'Dialog', der das ganze Rezept anzeigt. \textit{ResultList.vue} ist die Hauptkomponente für die Kommunikation mit dem Backend.
Von hier aus gehen die Suchanfragen ans Backend, die Ergebnisse werden empfangen und Tracking- sowie Evaluationswerte werden ebenfalls ans Backend gesendet.

\subsection{Backend}
Im Backend wird das Webframework \textit{Flask} verwendet. Es liefert den statisch erzeugten Build des Frontends aus, leitet Suchanfragen an Elasticsearch weiter und bietet die grundlegende Funktionalität, Suchanfragen zu tracken, sowie Relevanzurteile zu Dokumenten zu speichern. Hier sind die wesentlichen Routen, die das Backend zur Verfügung stellt:

\begin{itemize}
	\item \textbf{/} \\ Diese Route liefert das Frontend aus.
	\item \textbf{/search} \\ Unter dieser Route werden Suchanfragen entgegengenommen, an Elasticsearch weitergeleitet und die entsprechenden Suchergebnisse zurückgegeben. Außerdem wird die eingegebene Query getrackt.
	\item \textbf{/track/hit} \\ Zu einer bereits getrackten Query werden beim Aufrufen dieser Route die angeklickten Suchergebnisse gespeichert.
	\item \textbf{/track/time} \\ Hier wird zu einem angeklickten Suchergebnis die Dwelltime gespeichtert.
	\item \textbf{/relevance-judgment} \\ Werden von Usern \textit{Relevance-Judgments} abgegeben, so werden diese im aktuell bewerteten Dokument gespeichert.
	\item \textbf{/autocomplete} \\ Damit Suchvorschläge zu einer (noch nicht vollständig) eingegebenen Query angeboten werden können, leitet das Backend die an diese Route gesendeten Zeichen an Elasticserch weiter und antwortet den von Elasticsearch vorgeschlagenen\footnote{https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters.html} Zeichenketten.
	\item \textbf{/format-result} \\ Die Suchergebnisse zu vordefinierten Suchanfragen werden bezüglich ihrer \textit{Relevance-Judgments} gesammelt und in einer Evaluationsdatei hinterlegt (Siehe Kapitel \ref{sec:ranking-analysis}).
\end{itemize}

\subsection{Tracking}

Das Projekt beinhaltet Funktionalität zum Tracken der Interaktion der User mit der Suchmaschine. Genauer werden Daten über das Suchverhalten und das Verhalten beim Browsen der Ergebnisse pro Suchanfrage aufgezeichnet, nämlich das Datum, die Query, Treffer (\textit{Hits}) und die Verweilzeit (\textit{Dwelltime}). Entsprechend gliedert sich der Tracking-Vorgang in drei Phasen:

\begin{enumerate}
	\item Beim Eintreffen einer Suchanfrage wird für diese ein neuer Tracking-Eintrag hinterlegt.
	\item Klickt der User beim Browsen der Ergebnisse auf ein Dokument, wird sowohl am Tracking-Eintrag ein Verweis auf das Dokument gespeichert, als auch ein Trefferzähler am Dokument selbst inkrementiert.
	\item Wird das in Phase zwei geöffnete Dokument wieder geschlossen, wird die Zeit, die seit Beginn von Phase zwei verstrichen ist, ebenfalls am Tracking-Eintrag vermerkt.
\end{enumerate}

Beim Öffnen weiterer Dokumente wiederholen sich Phase zwei und drei. Beim Abschicken einer neuen (wenn auch identischen) Suchanfrage wird wieder bei Phase eins begonnen.
Daraus ergibt sich das in Abbildung \ref{lst:log-entry} dargestellte Datenmodell. Einträge dieser Art werden in einem zweiten \textit{ElasticSearch}-Index, dem \textit{Query-Store} gespeichert.

\begin{figure}[htb]
	\centering
	\lstinputlisting[language=json,firstnumber=1]{log-entry.json}
	\caption{Ein Tracking-Eintrag}
	\label{lst:log-entry}
\end{figure}

\pagebreak

\section{Evaluation}

\subsection{Performance- und Ranking-Analyse\label{sec:ranking-analysis}}

Die Auswertung der Suchmaschine geschieht durch die Berechnung von gängigen Effektivitätsmaßen.
Ausgehend von den folgenden vier verschiedenen \textit{Information Needs}\footnote{Im Repository unter \texttt{docs/evaluation/adhoc-retrieval-tasks.xml}} wird ein \textit{Adhoc-Top-10-Retrieval} mit der Suchmaschine durchgeführt.

\begin{enumerate}
	\item \textbf{"quick recipe potatoe onion tomato"} \\
	      \textit{As a broke student I would like to prepare a dish with potatoes, onions and tomatoes in under 20 minutes.}
	\item \textbf{"porridge gochi berry banana high carb"} \\
	      \textit{As a vegan I would like to prepare a porridge including gochi berries and bananas to match high carb diet.}
	\item \textbf{"lunch low carb less than 500 kcal"} \\
	      \textit{As a person tracking nutrition facts I would like to prepare lunch with less than 500kcal matching low carb diet.}
	\item \textbf{"low sugar diet ice cream"} \\
	      \textit{As a diabetes sufferer I want to prepare a low sugar ice cream as a dessert.}
\end{enumerate}

Für die Erhebung der \textit{Relevance Judgments} sind im Frontend Eingabemöglichkeiten für die Daten der Testperson (Name, Aufgabe) und deren Feedback zu jedem Suchergebnis (relevant, irrelevant) eingerichtet.
Über eine entsprechende Route im Backend werden die so ermittelten Daten im \textit{Document-Store} jeweils am Dokument, wie in Abbildung \ref{lst:relevance-judgment} veranschaulicht, gespeichert.

\begin{figure}[htb]
	\centering
	\lstinputlisting[language=json,firstnumber=1]{relevance-judgment.json}
	\caption{Ausschnitt eines Dokuments mit \textit{hitCount} und \textit{Relevance Judgment}}
	\label{lst:relevance-judgment}
\end{figure}

Die erhobenen \textit{Relevance Judgments} interpretieren wir als \textit{Ground-Truth}, die wir nun für die Bestimmung der gängigen Effektivitätsmaße heranziehen können. Für die Erhebung der Effektivitätsmaße verwenden wir das Evaluationstool \textit{TREC-Eval}\footnote{https://github.com/usnistgov/trec\_eval} der \textit{TREC}\footnote{https://trec.nist.gov/}. Die Ergebnisse sind in Abbildung \ref{tbl:results} dargestellt. An dieser Stelle sei angemerkt, dass die \textit{recall@k}-Werte für die ersten beiden Tasks stets $0$ sind, da sich unter den top $10$ Ergebnissen gemäß unserer Ground-Truth keine relevanten Suchergebnisse befanden. Daher nimmt auch der durschschnittliche \textit{recall@10} den Wert $0.5$ an.

Die Effektivität der von uns implementierten Suchmaschine unter den gegebenen Retrieval-Tasks ist schlecht. Sowohl \textit{Recall}, als auch die \textit{Precision} spielen gerade für unseren anfangs formulierten \textit{Information Need} eine entscheidende Rolle: Unter den obersten Ergebnissen möchte man als Nutzer vor allem wenig irrelevante Rezepte angezeigt bekommen, um möglichst bald mit der Zubereitung einer passenden Mahlzeit beginnen zu können.

\begin{figure}[htbp]
	\centering
	\subfloat[Durchschnittswerte über alle Tasks]{
		\begin{tabular}{ l r }
			\toprule
			$|Queries|$                                         & 4      \\
			$\sum_{q\,\in\,Queries}|Results_q|$                 & 40     \\
			$\sum_{q\,\in\,Queries}|Relevant_q|$                & 6      \\
			$\sum_{q\,\in\,Queries}|Relevant_q \cap Results_q|$ & 6      \\
			\midrule
			$precision@1$                                       & 0.2500 \\
			$precision@2$                                       & 0.1250 \\
			$precision@3$                                       & 0.0833 \\
			$precision@4$                                       & 0.0625 \\
			$precision@5$                                       & 0.0500 \\
			$precision@6$                                       & 0.1250 \\
			$precision@7$                                       & 0.1429 \\
			$precision@8$                                       & 0.1250 \\
			$precision@9$                                       & 0.1389 \\
			$precision@10$                                      & 0.1500 \\
			\midrule
			$recall@1$                                          & 0.0625 \\
			$recall@2$                                          & 0.0625 \\
			$recall@3$                                          & 0.0625 \\
			$recall@4$                                          & 0.0625 \\
			$recall@5$                                          & 0.0625 \\
			$recall@6$                                          & 0.2500 \\
			$recall@7$                                          & 0.3750 \\
			$recall@8$                                          & 0.3750 \\
			$recall@9$                                          & 0.4375 \\
			$recall@10$                                         & 0.5000 \\
			\midrule
			$MAP$                                               & 0.1857 \\
			\bottomrule
		\end{tabular}
	}
	\subfloat[Recall-Werte pro Task]{
		\begin{tabular}{ l | r r r r r r}
			\toprule
			Task        & 1 & 2 & 3    & 4   \\
			\midrule
			$recall@1$  & 0 & 0 & 0.25 & 0   \\
			$recall@2$  & 0 & 0 & 0.25 & 0   \\
			$recall@3$  & 0 & 0 & 0.25 & 0   \\
			$recall@4$  & 0 & 0 & 0.25 & 0   \\
			$recall@5$  & 0 & 0 & 0.25 & 0   \\
			$recall@6$  & 0 & 0 & 0.5  & 0.5 \\
			$recall@7$  & 0 & 0 & 0.5  & 1   \\
			$recall@8$  & 0 & 0 & 0.5  & 1   \\
			$recall@9$  & 0 & 0 & 0.75 & 1   \\
			$recall@10$ & 0 & 0 & 1    & 1   \\
			\bottomrule
		\end{tabular}
	}
	\caption{Ergebnisse}
	\label{tbl:results}
\end{figure}


\pagebreak

\subsection{Ausblick}\label{sec:outlook}

Unsere Evaluation hat gezeigt, dass die Effektivität unserer Suchmaschine wesentlich besser sein kann. Zum einen sind die \textit{Information Needs}, die für das \textit{Adhoc-Retrieval} formuliert sind, sehr spezifisch, die Queries und die Verarbeitung derselben aber sehr generell gehalten. Insbesondere ist das einmalige \textit{Adhoc-Retrieval} auch insofern für die Evaluierung unrealistisch, als das man als User in der Regel durch Verfeinerung der Query bereits intuitiv manuell versuchen würde, die Qualität der Suchergebnisse kurzfristig zu erhöhen.

Ein möglicher nächster Schritt könnte also sein, zuerst eine repräsentativere Evaluierung zu planen und die Suchmaschine dann in mehreren Iteration gegen die entsprechenden neuen Tasks zu optimieren. Genauer sind für die technische Optimierung unter anderem folgende Ansätze denkbar:

\begin{itemize}
	\item Domain-spezifische Phrasenerkennung (z.B. "low carb", "high protein", \dots)
	\item Übersetzung von solchen Phrasen in implizite erweiterte Suchparameter
	\item Unscharfe Suche für exakte Nährwertangaben
	\item Bereichsuche für Nährwertangaben (obere und untere statt nur obere Schranken)
	\item Sortierfunktion für Sortierung nach Nährwertangaben im Web-Interface
	\item Zutatenfilter per Kategorie (vegan, glutenfrei, \dots)
\end{itemize}

\subsection{Schlussbetrachtung}

Im Rahmen des Information-Retrieval-Praktikums haben wir uns praktisch mit der Konzipierung und Konstruktion eines Informationssystems in Form einer Rezeptsuchmaschine beschäftigt. Nach der Beschaffung der Datengrundlage haben wir zu Zwecken der Datenanalyse und Implementierung verschiedene State-of-the-Art-Technologien herangezogen. Nicht zuletzt hat uns die quantitative Evaluierung der Suchmaschine gezeigt, dass die Präzision des Systems noch erheblich verbessert werden kann, wie wir im Ausblick in Kapitel \ref{sec:outlook} näher erläutern.

\end{document}

