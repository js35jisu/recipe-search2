// PREPARATION ===============================================================================

let fs = require('fs');
let eol = require('os').EOL;
let bulkDocumentStore = 'data/bulk-document-store.jsonl'; // used to fill the document store
fs.unlink(bulkDocumentStore, err => { });

// ACQUISITION ===============================================================================

let inputFile = 'data/full_format_recipes.json';
let recipeArrayJSON = fs.readFileSync(inputFile, { encoding: 'utf8' }).trim();

// TRANSFORMATION ============================================================================

var auto_increment = 0;
JSON.parse(recipeArrayJSON).forEach(document => {
    auto_increment++;

    // bulk document store
    op = {
        index: {
            _index: "document-store",
            _type: "_doc",
            _id: auto_increment
        }
    };
    fs.appendFileSync(bulkDocumentStore, JSON.stringify(op) + eol + JSON.stringify(document) + eol);
});