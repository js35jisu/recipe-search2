SELECT COUNT(*) AS incomplete_nutrion_data_count
FROM epi_r
WHERE
  calories IS NULL OR calories = '' OR
  protein IS NULL OR protein = '' OR
  fat IS NULL OR fat = '' OR
  sodium IS NULL OR sodium = ''
ORDER BY rating
  DESC;

SELECT COUNT(*) AS no_nutrion_data_count
FROM epi_r
WHERE
  (calories IS NULL OR calories = '') AND
  (protein IS NULL OR protein = '') AND
  (fat IS NULL OR fat = '') AND
  (sodium IS NULL OR sodium = '')
ORDER BY rating
  DESC;

SELECT COUNT(*) AS total_count
FROM epi_r;